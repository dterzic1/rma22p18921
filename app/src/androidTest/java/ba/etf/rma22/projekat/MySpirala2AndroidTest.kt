//package ba.etf.rma22.projekat
//
//import android.graphics.Color
//import android.view.View
//import android.widget.TextView
//import androidx.core.content.ContextCompat
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.assertion.ViewAssertions
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.matcher.BoundedMatcher
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.ext.junit.rules.ActivityScenarioRule
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma22.projekat.UtilTestClass.Companion.withTextColor
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository
//import org.hamcrest.CoreMatchers
//import org.hamcrest.Description
//import org.hamcrest.Matcher
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//
//    @get:Rule
//    val intentsTestRule = ActivityScenarioRule<MainActivity>(MainActivity::class.java)
//
//    //dodatni instrumented test za zadatak 1
//    @Test
//    fun upisNaIstrazivanje(){
//        Espresso.onView(ViewMatchers.withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
//        Espresso.onData(
//            CoreMatchers.allOf(
//                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                CoreMatchers.`is`("Sve moje ankete")
//            )
//        ).perform(ViewActions.click())
//        val velicina=AnketaRepository.getMyAnkete().size
//        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa)).check(UtilTestClass.hasItemCount(velicina))
//
//        Espresso.onView(ViewMatchers.withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
//        Espresso.onView(ViewMatchers.withId(R.id.dodajIstrazivanjeDugme)).perform(ViewActions.click())
//
//        Thread.sleep(500)
//
//        Espresso.onView(ViewMatchers.withSubstring("Uspješno ste upisani u grupu"))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//
//        Espresso.onView(ViewMatchers.withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa)).check(UtilTestClass.hasItemCount(velicina+1))
//    }
//
//    //dodatni test za zadatak 2
//    @Test
//    fun pamcenjeOdgovora(){
//        Espresso.onView(ViewMatchers.withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//        Espresso.onView(ViewMatchers.withId(R.id.filterAnketa)).perform(ViewActions.click())
//        Espresso.onData(
//            CoreMatchers.allOf(
//                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                CoreMatchers.`is`("Sve moje ankete")
//            )
//        ).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa)).perform(
//            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(
//                ViewMatchers.hasDescendant(ViewMatchers.withText("Anketa 7")),
//                ViewMatchers.hasDescendant(ViewMatchers.withText("Istrazivanje 4"))
//            ), ViewActions.click()
//            ))
//
//        Espresso.onData(
//            CoreMatchers.allOf(
//                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                CoreMatchers.`is`("Odgovor 1")
//            )
//        ).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.dugmeZaustavi)).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.listaAnketa)).perform(
//            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(
//                ViewMatchers.hasDescendant(ViewMatchers.withText("Anketa 7")),
//                ViewMatchers.hasDescendant(ViewMatchers.withText("Istrazivanje 4"))
//            ), ViewActions.click()
//            ))
//
//        Espresso.onView(ViewMatchers.withSubstring("Odgovor 1")).
//            check(matches(withTextColor(Color.parseColor("#0000FF"))))
//
//    }
//
//}