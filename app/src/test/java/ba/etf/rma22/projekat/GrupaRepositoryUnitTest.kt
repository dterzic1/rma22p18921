//package ba.etf.rma22.projekat
//
//import ba.etf.rma22.projekat.data.models.Grupa
//import ba.etf.rma22.projekat.data.repositories.GrupaRepository
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.CoreMatchers.not
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.beans.HasPropertyWithValue.hasProperty
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.junit.Assert.assertEquals
//import org.junit.Test
//
//class GrupaRepositoryUnitTest {
//
//
//    @Test
//    fun getGroupsByIstrazivanjeTest(){
//        val grupe=GrupaRepository.getGroupsByIstrazivanje("Istrazivanje 1")
//        assertEquals(grupe.size, 2)
//        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("Grupa 1.1"))))
//        assertThat(grupe, not(hasItem<Grupa>(hasProperty("naziv", Is("Grupa 3.1")))))
//    }
//
//    @Test
//    fun pomocneTest(){
//        var grupe=GrupaRepository.getMyGroups()
//        assertEquals(grupe.size, 1)
//        GrupaRepository.addGroup("Grupa 2.2")
//        grupe=GrupaRepository.getMyGroups()
//        assertEquals(grupe.size, 2)
//        GrupaRepository.removeGroup("Grupa 2.2")
//        grupe=GrupaRepository.getMyGroups()
//        assertEquals(grupe.size, 1)
//    }
//}