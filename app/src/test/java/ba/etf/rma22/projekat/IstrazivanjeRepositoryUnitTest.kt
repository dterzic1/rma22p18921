//package ba.etf.rma22.projekat
//
//import ba.etf.rma22.projekat.data.models.Anketa
//import ba.etf.rma22.projekat.data.models.Istrazivanje
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository
//import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.CoreMatchers.not
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.beans.HasPropertyWithValue.hasProperty
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.junit.Assert.assertEquals
//import org.junit.Test
//
//class IstrazivanjeRepositoryUnitTest {
//
//    @Test
//    fun pomocneTest(){
//        IstrazivanjeRepository.setPosljednjaGodina(3)
//        assertEquals(IstrazivanjeRepository.getPosljednjaGodina(), 3)
//        IstrazivanjeRepository.setPosljednjaGodina(1)
//        assertEquals(IstrazivanjeRepository.getPosljednjaGodina(), 1)
//
//        assertEquals(IstrazivanjeRepository.getUpisani().size, 1)
//        IstrazivanjeRepository.dodajIstrazivanje("Istrazivanje 1")
//        assertEquals(IstrazivanjeRepository.getUpisani().size, 2)
//        IstrazivanjeRepository.obrisiIstrazivanje("Istrazivanje 1")
//        assertEquals(IstrazivanjeRepository.getUpisani().size, 1)
//    }
//
//    @Test
//    fun getIstrazivanjeByGodinaTest(){
//        val istrazivanja=IstrazivanjeRepository.getIstrazivanjeByGodina(2)
//        assertEquals(istrazivanja.size, 1)
//        assertThat(istrazivanja, hasItem<Istrazivanje>(hasProperty("naziv", Is("Istrazivanje 2"))))
//        assertThat(istrazivanja, not(hasItem<Istrazivanje>(hasProperty("naziv", Is("Istrazivanje 3")))))
//    }
//
//    @Test
//    fun getAllTest(){
//        val istrazivanja=IstrazivanjeRepository.getAll()
//        assertEquals(istrazivanja.size, 4)
//        assertThat(istrazivanja, hasItem<Istrazivanje>(hasProperty("naziv", Is("Istrazivanje 2"))))
//        assertThat(istrazivanja, hasItem<Istrazivanje>(hasProperty("naziv", Is("Istrazivanje 3"))))
//    }
//
//    @Test
//    fun getUpisaniTest(){
//        val istrazivanja=IstrazivanjeRepository.getUpisani()
//        assertEquals(istrazivanja.size, 1)
//        assertThat(istrazivanja, hasItem<Istrazivanje>(hasProperty("naziv", Is("Istrazivanje 4"))))
//        assertThat(istrazivanja, not(hasItem<Istrazivanje>(hasProperty("naziv", Is("Istrazivanje 2")))))
//    }
//
//
//
//}