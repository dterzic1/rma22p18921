//package ba.etf.rma22.projekat
//
//
//import ba.etf.rma22.projekat.data.models.Anketa
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository
//import org.hamcrest.CoreMatchers.hasItem
//import org.hamcrest.CoreMatchers.not
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.beans.HasPropertyWithValue.hasProperty
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.junit.Assert.assertEquals
//import org.junit.Test
//
//class AnketaRepositoryUnitTest {
//
//    @Test
//    fun testGetMyAnkete(){
//        val ankete=AnketaRepository.getMyAnkete()
//        assertEquals(ankete.size, 1)
//        assertThat(ankete, hasItem<Anketa>(hasProperty("naziv", Is("Anketa 7"))))
//        assertThat(ankete, not(hasItem<Anketa>(hasProperty("nazivIstrazivanja", Is("Istrazivanje 1")))))
//    }
//
//    @Test
//    fun testGetAll(){
//        val ankete=AnketaRepository.getAll()
//        assertEquals(ankete.size, 7)
//        assertThat(ankete, hasItem<Anketa>(hasProperty("naziv", Is("Anketa 7"))))
//        assertThat(ankete, hasItem<Anketa>(hasProperty("nazivIstrazivanja", Is("Istrazivanje 1"))))
//    }
//
//    @Test
//    fun testGetDone(){
//        val ankete=AnketaRepository.getDone()
//        assertEquals(ankete.size, 0)
//        assertThat(ankete, not(hasItem<Anketa>(hasProperty("nazivIstrazivanja", Is("Istrazivanje 4")))))
//    }
//
//    @Test
//    fun testGetFuture(){
//        val ankete=AnketaRepository.getFuture()
//        assertEquals(ankete.size, 1)
//        assertThat(ankete, hasItem<Anketa>(hasProperty("nazivIstrazivanja", Is("Istrazivanje 4"))))
//    }
//
//    @Test
//    fun testGetNotTaken(){
//        val ankete=AnketaRepository.getNotTaken()
//        assertEquals(ankete.size, 0)
//        assertThat(ankete, not(hasItem<Anketa>(hasProperty("nazivIstrazivanja", Is("Istrazivanje 4")))))
//    }
//}