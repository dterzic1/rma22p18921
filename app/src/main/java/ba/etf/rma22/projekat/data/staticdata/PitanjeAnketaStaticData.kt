//package ba.etf.rma22.projekat.data.staticdata
//
//import ba.etf.rma22.projekat.data.models.PitanjeAnketa
//
//fun PitanjaAnkete() : List<PitanjeAnketa>{
//    return listOf(
//        PitanjeAnketa("Pitanje 1", "Anketa 1", "Istrazivanje 1", null),
//        PitanjeAnketa("Pitanje 3", "Anketa 1", "Istrazivanje 1", null),
//        PitanjeAnketa("Pitanje 5", "Anketa 1", "Istrazivanje 1", null),
//        PitanjeAnketa("Pitanje 7", "Anketa 1", "Istrazivanje 1", null),
//
//        PitanjeAnketa("Pitanje 1", "Anketa 2", "Istrazivanje 1", null),
//        PitanjeAnketa("Pitanje 2", "Anketa 2", "Istrazivanje 1", null),
//        PitanjeAnketa("Pitanje 3", "Anketa 2", "Istrazivanje 1", null),
//        PitanjeAnketa("Pitanje 4", "Anketa 2", "Istrazivanje 1", null),
//
//        PitanjeAnketa("Pitanje 1", "Anketa 3", "Istrazivanje 2", null),
//        PitanjeAnketa("Pitanje 8", "Anketa 3", "Istrazivanje 2", null),
//        PitanjeAnketa("Pitanje 6", "Anketa 3", "Istrazivanje 2", null),
//
//        PitanjeAnketa("Pitanje 2", "Anketa 4", "Istrazivanje 2", null),
//        PitanjeAnketa("Pitanje 4", "Anketa 4", "Istrazivanje 2", null),
//        PitanjeAnketa("Pitanje 7", "Anketa 4", "Istrazivanje 2", null),
//
//        PitanjeAnketa("Pitanje 5", "Anketa 5", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 6", "Anketa 5", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 7", "Anketa 5", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 8", "Anketa 5", "Istrazivanje 3", null),
//
//        PitanjeAnketa("Pitanje 1", "Anketa 6", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 2", "Anketa 6", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 3", "Anketa 6", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 4", "Anketa 6", "Istrazivanje 3", null),
//        PitanjeAnketa("Pitanje 6", "Anketa 6", "Istrazivanje 3", null),
//
//        PitanjeAnketa("Pitanje 1", "Anketa 7", "Istrazivanje 4", null),
//        PitanjeAnketa("Pitanje 3", "Anketa 7", "Istrazivanje 4", null),
//        PitanjeAnketa("Pitanje 4", "Anketa 7", "Istrazivanje 4", null),
//        PitanjeAnketa("Pitanje 8", "Anketa 7", "Istrazivanje 4", null)
//
//    )
//}