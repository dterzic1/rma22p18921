package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Anketa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.http.Path
import java.util.*

object AnketaRepository {
    val db = RMA22DB.getInstance(MainActivity.mojContext)


    suspend fun getAll(offset: Int = 0) : List<Anketa>{
        return withContext(Dispatchers.IO) {
            if (offset != 0) {
                var response = ApiAdapter.retrofit.getAll(offset)
                val responseBody = response.body()!!
                db.anketaDao().deleteAll()
                db.anketaDao().insertAll(*responseBody.toTypedArray())
                return@withContext responseBody
            } else {
                var lista = arrayListOf<Anketa>()
                var i=1
                do{
                    var response = ApiAdapter.retrofit.getAll(i)
                    lista.addAll(response.body()!!)
                    i++
                }while(response.body()!!.size==5)
                db.anketaDao().deleteAll()
                db.anketaDao().insertAll(*lista.toTypedArray())

                return@withContext lista
            }
        }
    }


    suspend fun getBasBasAll() : List<Anketa>{
        return withContext(Dispatchers.IO){
            var ankete = getAll()
            var povratneAnkete = arrayListOf<Anketa>()
            ankete.forEach { anketa ->
                val istrazivanja = IstrazivanjeIGrupaRepository.getIstrazivanjaZaAnketu(anketa.id)
                istrazivanja.forEach{ istrazivanje ->
                    var grupe = IstrazivanjeIGrupaRepository.getGrupeZaIstrazivanje(istrazivanje.id)
                    val grupe2 = IstrazivanjeIGrupaRepository.getGrupeZaAnketu(anketa.id)
                    grupe = grupe.intersect(grupe2).toList()
                    grupe.forEach {
                        val anketa1 = Anketa(anketa.id, anketa.naziv,anketa.datumPocetak,anketa.datumKraj,anketa.trajanje,null)
                        anketa1.nazivIstrazivanja = istrazivanje.naziv
                        povratneAnkete.add(anketa1)
                    }
                }
            }
            db.anketaDao().deleteAll()
            db.anketaDao().insertAll(*povratneAnkete.toTypedArray())
            return@withContext povratneAnkete
        }
    }

    suspend fun getById(anketaId: Int) : Anketa?{
        return withContext(Dispatchers.IO){
            var response = ApiAdapter.retrofit.getById(anketaId)
            val responseBody = response?.body()
            return@withContext responseBody
        }
    }

    suspend fun getIdByAnketaTakenId(anketaTakenId : Int) : Int{
        return withContext(Dispatchers.IO){
            var response= TakeAnketaRepository.getPoceteAnkete()!!.filter { it.id==anketaTakenId }.last().AnketumId
            return@withContext response
        }
    }

    suspend fun getUpisane() : List<Anketa>{
        return withContext(Dispatchers.IO){
            var response = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())
            val responseBody = response.body()
            var lista = arrayListOf<Anketa>()
            responseBody!!.forEach{
                var response1 = ApiAdapter.retrofit.getAnketeZaGrupu(it.id)
                lista.addAll(response1.body()!!)
            }
            return@withContext lista
        }
    }

    suspend fun isTaken(idAnkete: Int) : Boolean{
        return withContext(Dispatchers.IO){
            var upisane= getUpisane()
            return@withContext upisane.any{it.id==idAnkete}
        }
    }

    suspend fun getProgres(idAnkete: Int) : Int{
        val progres= (OdgovorRepository.getOdgovoriAnketa(idAnkete).size + 0.0)/ PitanjeAnketaRepository.getPitanja(idAnkete).size
        return OdgovorRepository.sredi(progres)
    }
//
//        fun getMyAnkete() : List<Anketa> {
//        //  sve ankete za istraživanja i grupe gdje je korisnik upisan
//        return ankete.filter{anketa -> IstrazivanjeRepository.getUpisani().
//            map{istrazivanje ->  istrazivanje.naziv}.contains(anketa.nazivIstrazivanja)}.
//            filter { anketa -> GrupaRepository.getMyGroups().contains(anketa.nazivGrupe) }
//    }
//
//
//    fun getAll(): List<Anketa> {
//        return ankete
//    }
//
//    fun getDone() : List<Anketa> {
//        return getMyAnkete().filter{anketa -> predaneAnkete.contains(Pair(anketa.naziv, anketa.nazivIstrazivanja))}
//    }
//
//    fun getFuture() : List<Anketa> {
//        return getMyAnkete().filter{anketa-> !getDone().contains(anketa)}.filter{anketa -> anketa.datumKraj>Calendar.getInstance().time }
//    }
//
//    fun getNotTaken() : List<Anketa>{
//        return getMyAnkete().filter{anketa -> anketa.datumKraj<Calendar.getInstance().time && !getDone().contains(anketa)}
//    }
//
//    fun addPredanaAnketa(anketa : Pair<String, String>){
//        if(! predaneAnkete.contains(anketa))
//            predaneAnkete.add(anketa)
//    }
//
//    fun getAnketa(anketa1 : Pair<String, String>) : Anketa{
//        return ankete.find { anketa -> anketa.naziv==anketa1.first && anketa.nazivIstrazivanja==anketa1.second}!!
//    }
//
//    fun getZelene() : List<Anketa>{
//        val vrijeme = Calendar.getInstance().time
//        return ankete.filter{anketa -> anketa.datumPocetak<vrijeme && anketa.datumKraj>vrijeme && !getDone().contains(anketa)}
//    }
////
////    fun azurirajProgres(anketa1 : Anketa){
////        val progres= (PitanjeAnketaRepository.getOdgovorenaPitanja(anketa1.naziv, anketa1.nazivIstrazivanja).size.toFloat()/
////                PitanjeAnketaRepository.getPitanja(anketa1.naziv, anketa1.nazivIstrazivanja).size.toFloat())
////        getAnketa(Pair(anketa1.naziv,anketa1.nazivIstrazivanja)).progres=progres
////    }
//
//    fun azurirajDatumRada(anketa1 : Anketa){
//        getAnketa(Pair(anketa1.naziv,anketa1.nazivIstrazivanja)).datumRada=Calendar.getInstance().time
//    }

}