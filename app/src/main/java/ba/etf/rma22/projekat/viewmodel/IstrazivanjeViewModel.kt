//package ba.etf.rma22.projekat.viewmodel
//
//
//import ba.etf.rma22.projekat.data.models.Istrazivanje
//import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
//
//class IstrazivanjeViewModel {
//
//    fun getPosljednjaGodina() : Int{
//        return IstrazivanjeRepository.getPosljednjaGodina()
//    }
//
//    fun setPosljednjaGodina(god: Int){
//        IstrazivanjeRepository.setPosljednjaGodina(god)
//    }
//
//    fun dodajIstrazivanje(istrazivanje: String){
//        IstrazivanjeRepository.dodajIstrazivanje(istrazivanje)
//    }
//    fun obrisiIstrazivanje(istrazivanje: String){
//        IstrazivanjeRepository.obrisiIstrazivanje(istrazivanje)
//    }
//
//
//    fun getIstrazivanjeByGodina(godina:Int) : List<Istrazivanje> {
//        return IstrazivanjeRepository.getIstrazivanjeByGodina(godina)
//    }
//
//    fun getAll(): List<Istrazivanje> {
//        return IstrazivanjeRepository.getAll()
//    }
//
//    fun getUpisani() : List<Istrazivanje> {
//        return IstrazivanjeRepository.getUpisani()
//    }
//}