package ba.etf.rma22.projekat.view


interface Communicator {
    fun passData(poruka : String)

    fun passAnketa(idAnketa : Int, istrazivanje: String)

    fun zaustavi()
}