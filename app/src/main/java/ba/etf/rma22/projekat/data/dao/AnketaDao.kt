package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa

@Dao
interface AnketaDao {

    @Query("SELECT * FROM Anketa")
    suspend fun getAllAnkete() : List<Anketa>

    @Query("DELETE FROM Anketa")
    suspend fun deleteAll() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg ankete: Anketa)

}