package ba.etf.rma22.projekat.data.dao

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {

    @Query("SELECT * FROM Pitanje")
    suspend fun getAllPitanja() : List<Pitanje>

    @Query("DELETE FROM Pitanje")
    suspend fun deleteAll() : Int

    @Delete
    suspend fun deletePitanja(vararg pitanja: Pitanje)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg pitanja: Pitanje)
}