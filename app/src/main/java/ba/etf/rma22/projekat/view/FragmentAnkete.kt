package ba.etf.rma22.projekat.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.TakeAnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import ba.etf.rma22.projekat.viewmodel.TakeAnketaViewModel
import java.util.*

class FragmentAnkete : Fragment(), AdapterView.OnItemSelectedListener {
    private lateinit var listaAnketa: RecyclerView
    private lateinit var listaAnketaAdapter: AnketaListAdapter
    private lateinit var spinnerFilteri: Spinner
    private var anketaListViewModel =  AnketaListViewModel(this@FragmentAnkete::nabavljanjeAnketaDone, this@FragmentAnkete::otvori)
    private lateinit var communicator: Communicator
    private var takeAnketaViewModel = TakeAnketaViewModel()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_ankete, container, false)
        listaAnketa = view.findViewById(R.id.listaAnketa)
        spinnerFilteri= view.findViewById(R.id.filterAnketa)
        communicator=activity as Communicator
        val opcije=listOf<String>("Sve moje ankete",
            "Sve ankete",
            "Urađene ankete",
            "Buduće ankete",
            "Prošle ankete")

        val filteriAdapter= ArrayAdapter<String>(
            requireActivity(),
            androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
            opcije
        )
        spinnerFilteri.adapter=filteriAdapter

        listaAnketa.layoutManager = GridLayoutManager(
            activity,
            2
        )
        listaAnketaAdapter = AnketaListAdapter(arrayListOf()){ anketa -> otvoriAnketu(anketa)}
        listaAnketa.adapter = listaAnketaAdapter

        spinnerFilteri.setOnItemSelectedListener(this)

        return view;
    }

    private fun otvoriAnketu(anketa : Anketa){
       anketaListViewModel.jeKorisnikovaAnketa(anketa) 
    }

    companion object {
        fun newInstance(): FragmentAnkete = FragmentAnkete()
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        val odabran= p0?.getItemAtPosition(position).toString()
        Toast.makeText(requireContext(), "malo strpljenja, učitavam", Toast.LENGTH_LONG).show()
        when (odabran){
            "Sve moje ankete" -> (anketaListViewModel.getMyAnkete())
            "Sve ankete" -> (anketaListViewModel.getAll())
            "Urađene ankete" -> (anketaListViewModel.getDone())
            "Buduće ankete" -> (anketaListViewModel.getFuture())
            "Prošle ankete" -> (anketaListViewModel.getNotTaken())
        }
    }

    fun nabavljanjeAnketaDone(ankete : List<Anketa>) {
        listaAnketaAdapter.updateAnkete(ankete)
    }

    fun otvori(pripada : Boolean, anketa : Anketa){
        if(!pripada)
            Toast.makeText(requireContext(), "nemate pristup anketi", Toast.LENGTH_SHORT).show()
        else if(pripada&& anketa.datumPocetak> Calendar.getInstance().time)
            Toast.makeText(requireContext(), "anketa nije otvorena", Toast.LENGTH_SHORT).show()
        else if(pripada){
            takeAnketaViewModel.zapocniAnketu(anketa.id) {
                communicator.passAnketa(anketa.id, anketa.nazivIstrazivanja!!)
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        //nece se desiti
    }


}