package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

object TakeAnketaRepository {
    private val db = RMA22DB.getInstance(MainActivity.mojContext)


    suspend fun zapocniAnketu(idAnkete:Int): AnketaTaken? {
        return withContext(Dispatchers.IO) {
            try {
                val pocete = getPoceteAnkete()
                if (pocete != null) {
                    val filtrirano =
                        pocete.filter { anketaTaken ->
                            anketaTaken.AnketumId == idAnkete && anketaTaken.student == AccountRepository.getHash()
                        }
                    if (filtrirano?.count() != 0)
                        return@withContext filtrirano!!.last()
                }
                var response = ApiAdapter.retrofit.zapocniAnketu(AccountRepository.getHash(), idAnkete)
                val responseBody = response.body()
                db.anketaTakenDao().insertAll(responseBody!!)
                return@withContext responseBody
            }
            catch(e: Exception){
                //ako nema interneta
                    if(!MainActivity.konektovano){
                        val pocete=db.anketaTakenDao().getAllAnketaTaken()
                        if (pocete.isNotEmpty()) {
                            val filtrirano =
                                pocete.filter { anketaTaken ->
                                    anketaTaken.AnketumId == idAnkete && anketaTaken.student == AccountRepository.getHash()
                                }
                            if (filtrirano?.count() != 0)
                                return@withContext filtrirano!!.last()
                        }
                    }
                return@withContext null
            }
        }
    }

    suspend fun getPoceteAnkete():List<AnketaTaken>?{
        return withContext(Dispatchers.IO){
            var response = ApiAdapter.retrofit.getPoceteAnkete(AccountRepository.getHash())
            val responseBody = response?.body()
            if(responseBody!!.isEmpty())
                return@withContext null
            return@withContext responseBody
        }
    }

    suspend fun getIdByAnketumId(anketaId : Int) : Int?{
        return withContext(Dispatchers.IO){
            var response= getPoceteAnkete() ?: return@withContext null
            try{
                val id= response.last { it.AnketumId==anketaId }.id
                return@withContext id
            }
            catch (e : Exception){
                return@withContext null
            }
        }
    }

    suspend fun getPokusajByAnketumId(anketaId : Int) : AnketaTaken?{
        return withContext(Dispatchers.IO){
            var response= getPoceteAnkete()?.last { it.AnketumId == anketaId }
            return@withContext response
        }
    }

}