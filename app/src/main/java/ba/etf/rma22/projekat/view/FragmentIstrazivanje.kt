package ba.etf.rma22.projekat.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.viewmodel.IstrazivanjeIGrupaViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FragmentIstrazivanje : Fragment() {
    private lateinit var dugme : Button
    private lateinit var grupe : Spinner
    private lateinit var communicator: Communicator
    private var grupeList = arrayListOf<String>("")
    private var istrazivanjeIGrupaViewModel = IstrazivanjeIGrupaViewModel()
    private var grupa=""
    private var istrazivanje=""
    private var odabranaGrupaId = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        var view= inflater.inflate(R.layout.fragment_istrazivanje, container, false)
        dugme=view.findViewById(R.id.dodajIstrazivanjeDugme)

        grupe=view.findViewById(R.id.odabirGrupa)
        istrazivanjeIGrupaViewModel.getNeupisaneGrupe { lista ->
            grupeList.addAll(lista.map { it.naziv })
        }

        var grupeAdapter= ArrayAdapter<String>(requireActivity(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, grupeList)
        grupe.adapter=grupeAdapter
        grupe.setSelection(0)

        grupe.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                dugme.isEnabled=false
                dugme.isClickable=false
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                dugme.isClickable = grupe.selectedItem!="" && MainActivity.konektovano
                dugme.isEnabled=dugme.isClickable
                if(dugme.isClickable) {
                    grupa = grupe.selectedItem as String
                    istrazivanjeIGrupaViewModel.getGrupaIdByNaziv(
                        grupa,
                        this@FragmentIstrazivanje::postaviOdabranuGrupuId
                    )
                    istrazivanjeIGrupaViewModel.getIstrazivanjeZaGrupu(
                        grupa,
                        this@FragmentIstrazivanje::postaviIstrazivanje
                    )
                }
            }
        }
        communicator= activity as Communicator

        dugme.setOnClickListener {
            istrazivanjeIGrupaViewModel.upisiUGrupu(odabranaGrupaId,
                this@FragmentIstrazivanje::onSuccess, this@FragmentIstrazivanje::onError)
        }
        return view
    }

    fun onSuccess(){
        communicator.passData("Uspješno ste upisani u grupu $grupa istraživanja $istrazivanje!")
    }

    fun onError(){
        communicator.passData("Došlo je do greške pri upisu u grupu $grupa istraživanja $istrazivanje!")
    }

    fun postaviOdabranuGrupuId( g : Int){
        this.odabranaGrupaId = g
    }

    fun postaviIstrazivanje( s : String){
        this.istrazivanje = s
    }

    companion object {
        fun newInstance(): FragmentIstrazivanje = FragmentIstrazivanje()
    }


}