package ba.etf.rma22.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import ba.etf.rma22.projekat.viewmodel.TakeAnketaViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class AnketaListAdapter (
    private var ankete: List<Anketa>,
    private val onItemClicked: (anketa : Anketa) -> Unit
    ) : RecyclerView.Adapter<AnketaListAdapter.AnketaViewHolder>() {
    private val anketaListViewModel = AnketaListViewModel(null,null)
    private val takeAnketaViewModel = TakeAnketaViewModel()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnketaViewHolder {
            val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_anketa, parent, false)
            return AnketaViewHolder(view)
        }
        override fun getItemCount(): Int = ankete.size
        override fun onBindViewHolder(holder: AnketaViewHolder, position: Int) {
            holder.anketaTitle.text = ankete[position].naziv
            holder.istrazivanjeTitle.text = ankete[position].nazivIstrazivanja
            var progres = 0
            anketaListViewModel.postaviProgres(ankete[position].id){ p ->
                progres=p
                holder.anketaProgres.setProgress(progres)

                anketaListViewModel.isTaken(ankete[position].id) { b->
                    val status= odrediStatus(position, progres, b)
                    when(status){
                        "plava" -> takeAnketaViewModel.getPokusajByAnketumId(ankete[position].id){ pokusaj->
                            holder.anketaDatum.text = "Anketa urađena:\n" + formatiraj(pokusaj.datumRada)
                        }
                        "zelena" -> holder.anketaDatum.text = "Vrijeme zatvaranja:\n"+ formatiraj(ankete[position].datumKraj)
                        "crvena" -> holder.anketaDatum.text = "Anketa zatvorena:\n" + formatiraj(ankete[position].datumKraj)
                        "zuta" -> holder.anketaDatum.text = "Vrijeme aktiviranja:\n" + formatiraj(ankete[position].datumPocetak)
                    }

                    //Pronalazimo id drawable elementa na osnovu statusa ankete
                    val context: Context = holder.statusImage.getContext()
                    var id: Int = context.getResources()
                        .getIdentifier(status, "drawable", context.getPackageName())
                    holder.statusImage.setImageResource(id)
                }

            }

            holder.itemView.setOnClickListener { onItemClicked(ankete[position]) }
        }

    private fun odrediStatus(position: Int, progres: Int, radjena: Boolean) : String{


        val kraj = ankete[position].datumKraj
        val danas = Calendar.getInstance().time
        if(radjena){
            if(kraj!=null && kraj < danas || progres==100)
                return "plava"
            else if ((kraj==null || kraj > danas) && progres<100)
                return "zelena"
        }
        else{
            if(ankete[position].datumPocetak > danas)
                return "zuta"
            if(kraj!=null && kraj < danas)
                return "crvena"
            //return "zelena"
        }
        return "zelena"
    }

    private fun formatiraj(date: Date?) : String{
        if (date != null) {
            var dan=date.date
            var mjesec=date.month +1
            var godina=date.year + 1900
            var ispis: String =""
            if(dan<10)
                ispis="0"
            ispis= "$ispis$dan."
            if(mjesec<10)
                ispis += "0"
            ispis="$ispis$mjesec.$godina"
            return ispis
        }
        return "neodređeno"
    }

    fun updateAnkete(ankete: List<Anketa>) {
            this.ankete = ankete
            notifyDataSetChanged()
        }
        inner class AnketaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val statusImage: ImageView = itemView.findViewById(R.id.status)
            val anketaTitle: TextView = itemView.findViewById(R.id.nazivAnkete)
            val istrazivanjeTitle: TextView = itemView.findViewById(R.id.nazivIstrazivanja)
            val anketaDatum: TextView = itemView.findViewById(R.id.datum)
            val anketaProgres: ProgressBar = itemView.findViewById(R.id.progresZavrsetka)
        }


    }