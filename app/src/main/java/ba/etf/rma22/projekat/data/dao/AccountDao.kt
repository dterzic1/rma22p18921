package ba.etf.rma22.projekat.data.dao

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Account

@Dao
interface AccountDao {

    @Query("DELETE FROM Account")
    suspend fun deleteAccount() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAccount(account : Account) : Long

    @Query("SELECT * FROM Account")
    suspend fun getAll() : List<Account>

}