package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Pitanje(
    @PrimaryKey @SerializedName("id") val id: Int,
    @ColumnInfo(name="naziv") @SerializedName("naziv") val naziv: String, // - jedinstveni naziv pitanja u okviru ankete u kojoj se nalazi
    @ColumnInfo(name="tekstPitanja") @SerializedName("tekstPitanja") val tekstPitanja: String, // - tekst pitanja
    @ColumnInfo(name="opcije") @SerializedName("opcije") val opcije: List<String> //- lista ponuđenih odgovora
)
