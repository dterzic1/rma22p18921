package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {
    @Query("SELECT * FROM Odgovor")
    suspend fun getAllOdgovori() : List<Odgovor>

    @Query("DELETE FROM Odgovor")
    suspend fun deleteAll() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg odgovori: Odgovor)
}