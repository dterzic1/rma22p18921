package ba.etf.rma22.projekat.data.dao

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Grupa

@Dao
interface GrupaDao {

    @Query("SELECT * FROM Grupa")
    suspend fun getAllGrupe() : List<Grupa>

    @Query("DELETE FROM Grupa")
    suspend fun deleteAll() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg grupe: Grupa)
}