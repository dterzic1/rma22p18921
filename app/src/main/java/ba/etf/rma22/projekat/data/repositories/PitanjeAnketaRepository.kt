package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

object PitanjeAnketaRepository {
    private val db = RMA22DB.getInstance(MainActivity.mojContext)


    suspend fun getPitanja(idAnkete:Int):List<Pitanje>{
        return withContext(Dispatchers.IO) {
            var response = ApiAdapter.retrofit.getPitanja(idAnkete)
            val responseBody = response.body()!!
            db.pitanjeDao().deletePitanja(*responseBody.toTypedArray()) //za svaki sluc da se ne dupla sta
            db.pitanjeDao().insertAll(*responseBody.toTypedArray())
            return@withContext responseBody!!
        }
    }

    suspend fun getPitanjeById(idPitanja: Int, idAnkete: Int) : Pitanje{
        return withContext(Dispatchers.IO) {
            var response = getPitanja(idAnkete)
            return@withContext response.last { it.id == idPitanja }
        }
    }

    suspend fun isOdgovoreno(idPitanja: Int, idAnkete: Int) : Boolean{
        return withContext(Dispatchers.IO){
            var odgovori: List<Odgovor>
            if(MainActivity.konektovano)
                odgovori = OdgovorRepository.getOdgovoriAnketa(idAnkete)
            else
                odgovori=db.odgovorDao().getAllOdgovori()
            return@withContext odgovori.any{odgovor -> odgovor.PitanjeId==idPitanja }
        }
    }


//
//    fun getPitanja(nazivAnkete: String, nazivIstrazivanja: String): List<Pitanje>{
//        return pitanja.filter{pitanje ->
//                PitanjaAnkete().filter {pitanjeAnketa -> pitanjeAnketa.naziv.equals(pitanje.naziv)
//                    && pitanjeAnketa.anketa.equals(nazivAnkete)
//                    && pitanjeAnketa.istrazivanje.equals(nazivIstrazivanja)}
//                .isNotEmpty()}
//    }
//
//    fun getPitanje(nazivPitanja:String, nazivAnkete: String, nazivIstrazivanja: String): Pitanje{
//        return getPitanja(nazivAnkete, nazivIstrazivanja).find{ pitanje -> pitanje.naziv==nazivPitanja  }!!
//    }
//
//    fun odgovori(pitanjeAnketa: PitanjeAnketa, odgovor:Int) : Boolean{
//        var mijenjatProgres=false
//        val i=pitanjaAnkete.indexOf(pitanjeAnketa)
//        if(pitanjaAnkete[i].indeksOdgovora==null) mijenjatProgres=true
//        pitanjaAnkete[i].indeksOdgovora=odgovor
//
//        if(mijenjatProgres) odgovorenaPitanja.add(pitanjaAnkete[i])
//
//        return mijenjatProgres
//    }
//
//    fun getOdgovorenaPitanja(nazivAnkete: String, nazivIstrazivanja: String): List<Pitanje>{
//        return Pitanja().filter{pitanje ->
//            odgovorenaPitanja.filter {pitanjeAnketa -> pitanjeAnketa.naziv.equals(pitanje.naziv)
//                    && pitanjeAnketa.anketa.equals(nazivAnkete)
//                    && pitanjeAnketa.istrazivanje.equals(nazivIstrazivanja)}
//                .isNotEmpty()}
//    }
//
//    fun getPitanjeAnketa(naziv:String, anketa: String, istrazivanje : String) : PitanjeAnketa? {
//        return pitanjaAnkete.find{pitanjeAnketa ->
//                pitanjeAnketa.naziv==naziv
//                && pitanjeAnketa.anketa==anketa
//                && pitanjeAnketa.istrazivanje==istrazivanje}
//    }

}