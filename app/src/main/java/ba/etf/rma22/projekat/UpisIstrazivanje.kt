//package ba.etf.rma22.projekat
//
//import android.content.Intent
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import android.view.View
//import android.widget.AdapterView
//import android.widget.ArrayAdapter
//import android.widget.Button
//import android.widget.Spinner
//import ba.etf.rma22.projekat.viewmodel.GrupaViewModel
//import ba.etf.rma22.projekat.viewmodel.IstrazivanjeViewModel
//
//class UpisIstrazivanje : AppCompatActivity() {
//
//    private lateinit var dugme : Button
//    private lateinit var godine : Spinner
//    private lateinit var istrazivanja : Spinner
//    private lateinit var grupe : Spinner
//    private var istrazivanjeViewModel=IstrazivanjeViewModel()
//    private var grupaViewModel= GrupaViewModel()
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.upis_istrazivanje)
//        dugme=findViewById(R.id.dodajIstrazivanjeDugme)
//        godine=findViewById(R.id.odabirGodina)
//        istrazivanja=findViewById(R.id.odabirIstrazivanja)
//        grupe=findViewById(R.id.odabirGrupa)
//
//        val opcije= listOf<String>("1", "2", "3", "4", "5")
//        var godineAdapter=ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, opcije)
//        godine.adapter=godineAdapter
//        godine.setSelection(istrazivanjeViewModel.getPosljednjaGodina() -1)
//
//        var grupeAdapter=ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, listOf(""))
//        var istrazivanjaAdapter=ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, listOf(""))
//        grupe.adapter=grupeAdapter
//        istrazivanja.adapter=istrazivanjaAdapter
//
//        godine.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                dugme.isEnabled=false
//                dugme.isClickable=false
//                istrazivanjaAdapter.clear()
//                grupeAdapter.clear()
//            }
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                var istrazivanjaList=istrazivanjeViewModel.getIstrazivanjeByGodina(godine.selectedItemPosition+1).
//                    filterNot{i->istrazivanjeViewModel.getUpisani().contains(i)}.
//                    map{istrazivanje ->  istrazivanje.naziv}
//                if(istrazivanjaList.isEmpty()) {
//                    istrazivanjaList= listOf("")
//                }
//                istrazivanjaAdapter=ArrayAdapter<String>(this@UpisIstrazivanje, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, istrazivanjaList)
//                istrazivanja.adapter=istrazivanjaAdapter
//
//            }
//        }
//
//        istrazivanja.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                dugme.isEnabled=false
//                dugme.isClickable=false
//                grupeAdapter.clear()
//            }
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                var grupeList: List<String>
//                if(istrazivanja.selectedItem as String =="")
//                    grupeList= listOf("")
//                else
//                    grupeList = grupaViewModel.getGroupsByIstrazivanje(istrazivanja.selectedItem as String).map{grupa -> grupa.naziv }
//                if(grupeList.isEmpty()) {
//                    grupeList= listOf("")
//                }
//                grupeAdapter=ArrayAdapter<String>(this@UpisIstrazivanje, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, grupeList)
//                grupe.adapter=grupeAdapter
//            }
//        }
//
//        grupe.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
//            override fun onNothingSelected(p0: AdapterView<*>?) {
//                dugme.isEnabled=false
//                dugme.isClickable=false
//            }
//            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                dugme.isClickable = grupe.selectedItem!="" && istrazivanja.selectedItem!=""
//                dugme.isEnabled=dugme.isClickable
//            }
//
//        }
//
//        dugme.setOnClickListener {
//            istrazivanjeViewModel.dodajIstrazivanje(istrazivanja.selectedItem as String)
//            grupaViewModel.addGroup(grupe.selectedItem as String)
//            istrazivanjeViewModel.setPosljednjaGodina(godine.selectedItemPosition+1)
//            finish()
//        }
//    }
//}