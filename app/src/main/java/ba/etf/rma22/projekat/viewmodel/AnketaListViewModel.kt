package ba.etf.rma22.projekat.viewmodel

import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.view.AnketaListAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*


class AnketaListViewModel(private val nabavljanjeAnketaDone: ((ankete: List<Anketa>) -> Unit)?,
private val otvori: ((pripada: Boolean, anketa: Anketa) -> Unit)?) {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun isTaken(idAnketa: Int, onSuccess: ((Boolean) -> Unit)?){
        scope.launch {
            val rezultat = AnketaRepository.isTaken(idAnketa)
            onSuccess?.invoke(rezultat)
        }
    }

    //moguci null izuzetak
    fun getById(idAnketa: Int, onSuccess: ((Anketa) -> Unit)?){
        scope.launch {
            val rezultat = AnketaRepository.getById(idAnketa)!!
            onSuccess?.invoke(rezultat)
        }
    }

    fun dajProgresIdajAnketu(idAnkete : Int, onSuccess : ((Int, Anketa)->Unit)?){
        scope.launch {
            val rezultat = AnketaRepository.getProgres(idAnkete)
            val rezultat2 = AnketaRepository.getById(idAnkete)!!
            onSuccess?.invoke(rezultat, rezultat2)
        }
    }

    fun postaviProgres(idAnkete : Int, onSuccess : ((Int)->Unit)?) {
        scope.launch {
            val rezultat = AnketaRepository.getProgres(idAnkete)
            onSuccess?.invoke(rezultat)
        }
    }

    fun jeKorisnikovaAnketa(anketa: Anketa){
        scope.launch {
            var lista= AnketaRepository.getAll()
            val listaUpisanih = AnketaRepository.getUpisane()
            lista=lista.filter { listaUpisanih.any{upisana -> upisana.id==it.id} }
            val pripada = lista.any{it.id==anketa.id}
            otvori?.invoke(pripada, anketa)
        }
    }

    fun getMyAnkete() {
        scope.launch {
            var lista= AnketaRepository.getBasBasAll().sortedBy { it.datumPocetak }
            val listaUpisanih = AnketaRepository.getUpisane().sortedBy { it.datumPocetak }
            nabavljanjeAnketaDone?.invoke(lista.filter { listaUpisanih.any{upisana -> upisana.id==it.id} })
        }

    }

    fun getAll()  {
        scope.launch {
            val lista= AnketaRepository.getBasBasAll().sortedBy { it.datumPocetak }
            nabavljanjeAnketaDone?.invoke(lista)
        }


    }
    fun getDone()  {
        scope.launch {
            var lista= AnketaRepository.getBasBasAll().sortedBy { it.datumPocetak }
            val listaUpisanih = AnketaRepository.getUpisane().sortedBy { it.datumPocetak }
            var upisane=lista.filter { listaUpisanih.any{upisana -> upisana.id==it.id} }
            upisane=upisane.filter { it.datumKraj!= null && it.datumKraj<Calendar.getInstance().time
                            || AnketaRepository.getProgres(it.id)==100 }
            nabavljanjeAnketaDone?.invoke(upisane)
        }

    }

    fun getFuture(){
        scope.launch {
            var lista= AnketaRepository.getBasBasAll().sortedBy { it.datumPocetak }
            val listaUpisanih = AnketaRepository.getUpisane().sortedBy { it.datumPocetak }
            var upisane=lista.filter { listaUpisanih.any{upisana -> upisana.id==it.id} }
            upisane = upisane.filter {it.datumPocetak > Calendar.getInstance().time ||    //jos nije pocela -> zuta
                    ((it.datumKraj!= null && it.datumKraj > Calendar.getInstance().time || it.datumKraj== null) //jos nije gotova
                            && AnketaRepository.getProgres(it.id)<100) //->zelena
                    }
            nabavljanjeAnketaDone?.invoke(upisane)
        }

    }

    fun getNotTaken() {
        scope.launch {
            var lista= AnketaRepository.getBasBasAll().sortedBy { it.datumPocetak }
            val listaUpisanih = AnketaRepository.getUpisane().sortedBy { it.datumPocetak }
            var upisane=lista.filter { listaUpisanih.any{upisana -> upisana.id==it.id} }
            upisane = upisane.filter { ! AnketaRepository.isTaken(it.id) }
            nabavljanjeAnketaDone?.invoke(upisane)
        }

    }
}