package ba.etf.rma22.projekat.view

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.viewmodel.OdgovorViewModel
import ba.etf.rma22.projekat.viewmodel.PitanjeAnketaViewModel
import ba.etf.rma22.projekat.viewmodel.TakeAnketaViewModel


class FragmentPitanje: Fragment() {
    private lateinit var pitanjeTekst : TextView
    private lateinit var odgovori : ListView
    private lateinit var zaustaviDugme : Button
    private lateinit var communicator : Communicator
    private var idAnketa = 0
    private var idPitanja = 0
    private val takeAnketaViewModel = TakeAnketaViewModel()
    private val pitanjeAnketaViewModel = PitanjeAnketaViewModel()
    private val odgovorViewModel = OdgovorViewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_pitanje, container, false)
        pitanjeTekst=view.findViewById(R.id.tekstPitanja)
        odgovori=view.findViewById(R.id.odgovoriLista)
        zaustaviDugme=view.findViewById(R.id.dugmeZaustavi)
        communicator=activity as Communicator
        idPitanja=arguments?.getInt("pitanje")!!
        idAnketa=arguments?.getInt("anketa")!!

        var pitanje : Pitanje? = null
        var odgovoreno=false
        var idPokusaja=0
        takeAnketaViewModel.getIdByAnketumId(idAnketa){ id->
            idPokusaja=id
        }
        pitanjeAnketaViewModel.getPitanjeById(idPitanja, idAnketa){ p->
            pitanje = p
            pitanjeTekst.text=p.tekstPitanja
            val odgovoriAdapter= MyArrayAdapter(
                requireActivity(), androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
                p.opcije
            )
            odgovori.adapter=odgovoriAdapter
        }
        pitanjeAnketaViewModel.isOdgovoreno(idPitanja,idAnketa){ b->
            odgovoreno=b
            if(!b) {
                odgovori.isClickable=true
                odgovori.choiceMode = ListView.CHOICE_MODE_SINGLE
            }
            else odgovori.isClickable=false
        }


        odgovori.setOnItemClickListener { parent, view, position, id ->
                if(odgovori.isClickable) {
                    odgovorViewModel.postaviOdgovorAnketa(idPokusaja, idPitanja, position) {
                        (view as TextView).setTextColor(Color.parseColor("#0000FF"))
                        odgovori.isClickable = false
                    }
                }

        }

        zaustaviDugme.setOnClickListener {
            communicator.zaustavi()
        }

        return view
    }

    inner class MyArrayAdapter(context: Context, private val layoutResource: Int, private val elements: List<String>):
        ArrayAdapter<String>(context, layoutResource, elements) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = super.getView(position, convertView, parent)
            val textView = view.findViewById<TextView>(android.R.id.text1)
            odgovorViewModel.getOdabraniOdgovor(idPitanja,idAnketa){ odgovor->
                if(position==odgovor)
                    textView.setTextColor(Color.parseColor("#0000FF"))
                else
                    textView.setTextColor(Color.BLACK)
            }

            return view
        }
    }

    companion object {
        fun newInstance() : FragmentPitanje=FragmentPitanje()
    }
}