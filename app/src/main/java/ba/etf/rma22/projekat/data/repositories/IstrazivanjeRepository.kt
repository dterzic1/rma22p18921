//package ba.etf.rma22.projekat.data.repositories
//
//import ba.etf.rma22.projekat.data.staticdata.Istrazivanja
//import ba.etf.rma22.projekat.data.models.Istrazivanje
//
//object IstrazivanjeRepository {
//    private var mojiUpisani = mutableListOf<String>("Istrazivanje 4")
//    private var posljednjaGodina=1
//
//    fun getPosljednjaGodina() : Int{
//        return posljednjaGodina
//    }
//
//    fun setPosljednjaGodina(god: Int){
//        posljednjaGodina=god
//    }
//
//    fun dodajIstrazivanje(istrazivanje: String){
//        mojiUpisani.add(istrazivanje)
//    }
//    fun obrisiIstrazivanje(istrazivanje: String){
//        mojiUpisani.remove(istrazivanje)
//    }
//
//
//    fun getIstrazivanjeByGodina(godina:Int) : List<Istrazivanje> {
//        posljednjaGodina=godina
//        return Istrazivanja().filter { istrazivanje -> istrazivanje.godina.equals(godina)  }
//    }
//
//    fun getAll(): List<Istrazivanje> {
//        return Istrazivanja()
//    }
//
//    fun getUpisani() : List<Istrazivanje> {
//        return Istrazivanja().filter { istrazivanje -> mojiUpisani.contains(istrazivanje.naziv) }
//    }
//
//
//}