package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Istrazivanje

@Dao
interface IstrazivanjeDao {

    @Query("SELECT * FROM Istrazivanje")
    suspend fun getAllIstrazivanja() : List<Istrazivanje>

    @Query("DELETE FROM Istrazivanje")
    suspend fun deleteAll() : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg istrazivanja: Istrazivanje)
}