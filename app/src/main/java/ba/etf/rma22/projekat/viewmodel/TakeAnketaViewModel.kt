package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.repositories.TakeAnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class TakeAnketaViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)


    fun zapocniAnketu(idAnketa: Int, onSuccess: (() -> Unit)?){
        scope.launch {
            TakeAnketaRepository.zapocniAnketu(idAnketa)
            onSuccess?.invoke()
        }
    }

    fun getPokusajByAnketumId(idAnketa : Int, onSuccess : ((AnketaTaken)->Unit)?){
        scope.launch {
            val rezultat = TakeAnketaRepository.getPokusajByAnketumId(idAnketa)!!
            onSuccess?.invoke(rezultat)
        }
    }

    fun getIdByAnketumId(idAnketa: Int, onSuccess: ((Int) -> Unit)?){
        scope.launch {
            val rezultat = TakeAnketaRepository.getIdByAnketumId(idAnketa)!!
            onSuccess?.invoke(rezultat)
        }
    }
}