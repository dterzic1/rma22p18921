package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object AccountRepository {
    var acHash: String = "5b8a691d-2129-4c05-b03b-7454a2b908a5"
    private val db = RMA22DB.getInstance(MainActivity.mojContext)

    suspend fun postaviHash(acHash:String):Boolean{
        return withContext(Dispatchers.IO) {
            this@AccountRepository.acHash=acHash
            db.accountDao().deleteAccount()
            val rez = db.accountDao().addAccount(Account(acHash))    //vraca broj affected redova
            pobrisiSve()    //pobrisemo stare podatke
            return@withContext rez==1L
        }
    }

    suspend fun daSeNapraviBaza() : Boolean{
        return withContext(Dispatchers.IO){
            db.accountDao().getAll()
            return@withContext true
        }
    }

    fun getHash():String{
        return acHash
    }

    suspend fun pobrisiSve(){
        db.anketaTakenDao().deleteAll()
        db.anketaDao().deleteAll()
        db.odgovorDao().deleteAll()
        db.grupaDao().deleteAll()
        db.istrazivanjeDao().deleteAll()
        db.pitanjeDao().deleteAll()
    }
}

//https://rma22ws.herokuapp.com/androidDone/5b8a691d-2129-4c05-b03b-7454a2b908a5