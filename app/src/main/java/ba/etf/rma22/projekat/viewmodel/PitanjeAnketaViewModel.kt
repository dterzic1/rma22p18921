package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PitanjeAnketaViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getPitanja(idAnkete : Int, onSuccess: ((List<Pitanje>)->Unit)?){
        scope.launch {
            val rezultat = PitanjeAnketaRepository.getPitanja(idAnkete)
            onSuccess?.invoke(rezultat)
        }
    }

    fun getPitanjeById(idPitanja : Int, idAnkete: Int, onSuccess: ((Pitanje)->Unit)?){
        scope.launch {
            val rezultat = PitanjeAnketaRepository.getPitanjeById(idPitanja,idAnkete)
            onSuccess?.invoke(rezultat)
        }
    }

    fun isOdgovoreno(idPitanja : Int, idAnkete: Int, onSuccess: ((Boolean)->Unit)?){
        scope.launch {
            val rezultat = PitanjeAnketaRepository.isOdgovoreno(idPitanja,idAnkete)
            onSuccess?.invoke(rezultat)
        }
    }

}