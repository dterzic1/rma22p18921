package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.repositories.OdgovorRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class OdgovorViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun postaviOdgovorAnketa(idPokusaja : Int, idPitanja : Int, odg: Int, onSuccess : (()->Unit)){
        scope.launch {
            OdgovorRepository.postaviOdgovorAnketa(idPokusaja,idPitanja,odg)
            onSuccess?.invoke()
        }
    }

    fun getOdabraniOdgovor(idPitanja: Int, idAnkete: Int, onSuccess: (Int) -> Unit){
        scope.launch {
            val rezultat = OdgovorRepository.getOdabraniOdgovor(idPitanja,idAnkete)
            onSuccess?.invoke(rezultat)
        }
    }

}