package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.*
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.*

interface Api {

    @GET("/anketa/{id}/pitanja")
    suspend fun getPitanja(@Path("id") idAnkete : Int
    ): Response<List<Pitanje>>

    @POST("/student/{id}/anketa/{kid}")
    suspend fun zapocniAnketu(@Path("id") idStudenta: String,
        @Path("kid") idAnkete: Int
    ) : Response<AnketaTaken?>

    @GET("/student/{id}/anketataken")
    suspend fun getPoceteAnkete(@Path("id") idStudenta: String
    ) : Response<List<AnketaTaken>>?

    @GET("/student/{id}/anketataken/{ktid}/odgovori")
    suspend fun getOdgovoriAnketa(@Path("id") idStudenta: String,
        @Path("ktid") idPokusaja: Int
    ) : Response<List<Odgovor>>

    @POST("/student/{id}/anketataken/{ktid}/odgovor")
    suspend fun postaviOdgovorAnketa(@Path("id") idStudenta: String,
        @Path("ktid") idPokusaja: Int, @Body odgovor1: RequestBody
    ) : Response<Odgovor>

    @GET("/anketa")
    suspend fun getAll( @Query("offset") offset: Int
    ) : Response<List<Anketa>>

    @GET("/anketa/{id}")
    suspend fun getById(@Path("id") anketaId: Int
    ) : Response<Anketa>?

    @GET("/anketa/{id}/grupa")
    suspend fun getGrupeZaAnketu(@Path("id") id: Int
    ) : Response<List<Grupa>>

    @GET("/grupa/{id}/ankete")
    suspend fun getAnketeZaGrupu(@Path("id") id : Int
    ) : Response<List<Anketa>>

    @GET("/istrazivanje")
    suspend fun getIstrazivanja( @Query("offset") offset: Int
    ) : Response<List<Istrazivanje>>

    @GET("/grupa")
    suspend fun getGrupe() : Response<List<Grupa>>

    @GET("/grupa/{gid}/istrazivanje")
    suspend fun getIstrazivanjaZaGrupu(@Path("gid") idGrupe:Int
    ):Response<Istrazivanje?>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid") idGrupa:Int,
        @Path("id") idStudenta: String
    ) : Response<Poruka>

    @GET("/student/{id}/grupa")
    suspend fun getUpisaneGrupe(@Path("id") id : String
    ) : Response<List<Grupa>>
}