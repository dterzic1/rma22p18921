package ba.etf.rma22.projekat.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import java.util.*


class FragmentPredaj() : Fragment() {
    private lateinit var dugmePredaj : Button
    private lateinit var progresTekst : TextView
    private lateinit var communicator : Communicator
    private var odabranaAnketa : Anketa? = null
    private var anketaId = 0
    private val anketaListViewModel = AnketaListViewModel(null,null)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view= inflater.inflate(R.layout.fragment_predaj, container, false)
        dugmePredaj=view.findViewById(R.id.dugmePredaj)
        progresTekst=view.findViewById(R.id.progresTekst)
        communicator= activity as Communicator

        anketaId=arguments?.getInt("anketa")!!
        val istrazivanje=arguments?.getString("istrazivanje")

        anketaListViewModel.dajProgresIdajAnketu(anketaId){ p, a ->

            odabranaAnketa = a
            progresTekst.text="$p%"

            if(a.datumKraj!=null && a.datumKraj!! < Calendar.getInstance().time || p==100) {
                dugmePredaj.isEnabled = false
                dugmePredaj.isClickable=false
            } else{
                dugmePredaj.isEnabled = true
                dugmePredaj.isClickable=true
            }
        }


        dugmePredaj.setOnClickListener {
            communicator.passData("Završili ste anketu ${odabranaAnketa!!.naziv} u okviru istraživanja $istrazivanje")
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        anketaListViewModel.postaviProgres(anketaId){ p ->
            progresTekst.text="$p%"
        }
    }

    companion object {
        fun newInstance(): FragmentPredaj = FragmentPredaj()
    }

}