package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.dao.IstrazivanjeDao
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

object IstrazivanjeIGrupaRepository {
    private val db = RMA22DB.getInstance(MainActivity.mojContext)

    suspend fun getIstrazivanja(offset : Int = 0) : List<Istrazivanje>{
        return withContext(Dispatchers.IO){
            if(offset!=0) {
                var response = ApiAdapter.retrofit.getIstrazivanja(offset)
                val responseBody = response.body()!!
                db.istrazivanjeDao().deleteAll()
                db.istrazivanjeDao().insertAll(*responseBody.toTypedArray())
                return@withContext responseBody
            }
            else{
                var lista = arrayListOf<Istrazivanje>()
                var i=1
                do{
                    var response = ApiAdapter.retrofit.getIstrazivanja(i)
                    lista.addAll(response.body()!!)
                    i++
                }while(response.body()!!.size==5)
                db.istrazivanjeDao().deleteAll()
                db.istrazivanjeDao().insertAll(*lista.toTypedArray())
                return@withContext lista
            }
        }
    }

    suspend fun getNeupisaneGrupe() : List<Grupa>{
        return withContext(Dispatchers.IO){
            if(MainActivity.konektovano){
                var response = getGrupe() - getUpisaneGrupe()
                return@withContext response
            }
            else
                return@withContext db.grupaDao().getAllGrupe()
        }
    }

    suspend fun getGrupe() : List<Grupa>{
        return withContext(Dispatchers.IO){
            var response=ApiAdapter.retrofit.getGrupe()
            val responseBody=response.body()!!
            db.grupaDao().deleteAll()
            db.grupaDao().insertAll(*responseBody.toTypedArray())
            return@withContext responseBody
        }
    }

    suspend fun getGrupaByNaziv(nazivGrupe: String) : Int{
        return withContext(Dispatchers.IO){
            return@withContext getGrupe().find { it.naziv.equals(nazivGrupe) }!!.id
        }

    }

    suspend fun getIstrazivanjeZaGrupu(nazivGrupe: String) : Istrazivanje?{
        return withContext(Dispatchers.IO){
            val idGrupa= getGrupaByNaziv(nazivGrupe)
            var response=ApiAdapter.retrofit.getIstrazivanjaZaGrupu(idGrupa)
            val responseBody=response.body()
            return@withContext responseBody
        }
    }

    suspend fun getGrupeZaAnketu(idAnketa: Int) : List<Grupa>{
        return withContext(Dispatchers.IO){
            val response=ApiAdapter.retrofit.getGrupeZaAnketu(idAnketa)
            var responseBody=response.body()
            return@withContext responseBody.orEmpty()
        }
    }

    suspend fun getIstrazivanjaZaAnketu(idAnketa: Int) : List<Istrazivanje>{
        return withContext(Dispatchers.IO){
            val grupeZaAnketu= getGrupeZaAnketu(idAnketa)
            var istrazivanja = arrayListOf<Istrazivanje>()
            grupeZaAnketu.forEach{
                val istrazivanje= getIstrazivanjeZaGrupu(it.naziv)
                if(istrazivanje!=null && !(istrazivanja.contains(istrazivanje)))
                    istrazivanja.add(istrazivanje)
            }
            return@withContext istrazivanja
        }
    }

    suspend fun getGrupeZaIstrazivanje(idIstrazivanja:Int):List<Grupa>{
        return withContext(Dispatchers.IO){
            var listaGrupa= getGrupe()
            var povratnaLista = arrayListOf<Grupa>()
            listaGrupa.forEach {
                var response=ApiAdapter.retrofit.getIstrazivanjaZaGrupu(it.id)
                val responseBody=response.body()
                if(responseBody?.id==idIstrazivanja)
                    povratnaLista.add(it)
            }
            return@withContext povratnaLista
        }
    }

    suspend fun upisiUGrupu(idGrupa: Int) : Boolean{
        return withContext(Dispatchers.IO){
            var response=ApiAdapter.retrofit.upisiUGrupu(idGrupa, AccountRepository.getHash())
            val responseBody=response.body()
            return@withContext responseBody!!.message.contains("je dodan u grupu")
        }
    }

    suspend fun getUpisaneGrupe() :List<Grupa>{
        return withContext(Dispatchers.IO){
            var response=ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())
            val responseBody=response.body()
            return@withContext responseBody.orEmpty()
        }
    }
}