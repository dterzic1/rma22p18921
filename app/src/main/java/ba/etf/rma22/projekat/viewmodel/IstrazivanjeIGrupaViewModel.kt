package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class IstrazivanjeIGrupaViewModel {
    private val scope = CoroutineScope(Job() + Dispatchers.Main)


    fun getNeupisaneGrupe(onSuccess : ((List<Grupa>) -> Unit)?){
        scope.launch {
            val rezultat = IstrazivanjeIGrupaRepository.getNeupisaneGrupe()
            onSuccess?.invoke(rezultat)
        }
    }

    fun getIstrazivanjeZaGrupu(nazivGrupe : String, onSuccess: ((String)->Unit)?){
        scope.launch {
            val rezultat = IstrazivanjeIGrupaRepository.getIstrazivanjeZaGrupu(nazivGrupe)
            if(rezultat == null)
                onSuccess?.invoke("")
            else
                onSuccess?.invoke(rezultat.naziv)
        }
    }

    fun getGrupaIdByNaziv(nazivGrupe : String, onSuccess: ((Int) -> Unit)?){
        scope.launch {
            val rezultat = IstrazivanjeIGrupaRepository.getGrupaByNaziv(nazivGrupe)
            onSuccess?.invoke(rezultat)
        }
    }

    fun upisiUGrupu(idGrupa : Int, onSuccess: (() -> Unit)?, onError : (() -> Unit)?){
        scope.launch {
            val rezultat = IstrazivanjeIGrupaRepository.upisiUGrupu(idGrupa)
            if(rezultat)
                onSuccess?.invoke()
            else
                onError?.invoke()
        }
    }
}