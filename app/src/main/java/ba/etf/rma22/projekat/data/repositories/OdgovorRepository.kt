package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.ApiAdapter
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Odgovor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject

object OdgovorRepository {
    private val db = RMA22DB.getInstance(MainActivity.mojContext)

    suspend fun getOdgovoriAnketa(idAnkete: Int) :List<Odgovor>{
        return withContext(Dispatchers.IO){
            val idPokusaja= TakeAnketaRepository.getIdByAnketumId(idAnkete)
            if(idPokusaja==null) return@withContext emptyList()
            var response = ApiAdapter.retrofit.getOdgovoriAnketa(AccountRepository.getHash(), idPokusaja)
            val responseBody = response.body()
            return@withContext responseBody.orEmpty()
        }
    }

    suspend fun getOdabraniOdgovor(idPitanja: Int, idAnkete: Int) : Int {
        return withContext(Dispatchers.IO){
            var odgovori = getOdgovoriAnketa(idAnkete)
            if(PitanjeAnketaRepository.isOdgovoreno(idPitanja, idAnkete)) {
                return@withContext odgovori.last { odgovor -> odgovor.PitanjeId == idPitanja }.odgovoreno
            }
            return@withContext -1
        }
    }

    suspend fun postaviOdgovorAnketa(idAnketaTaken:Int,idPitanje:Int?,odgovor:Int):Int{
        return withContext(Dispatchers.IO){
            try {
                val idAnkete=AnketaRepository.getIdByAnketaTakenId(idAnketaTaken)
                val progres= (getOdgovoriAnketa(idAnkete).size +1.0)/ PitanjeAnketaRepository.getPitanja(idAnkete).size
                val sredjenProgres=sredi(progres)

                val jsonObject = JSONObject()
                jsonObject.put("odgovor", odgovor)
                jsonObject.put("pitanje", idPitanje)
                jsonObject.put("progres", sredjenProgres)
                val stringJson=jsonObject.toString()
                val body=RequestBody.create(MediaType.parse("application/json; charset=utf-8"),stringJson)

                ApiAdapter.retrofit.postaviOdgovorAnketa(
                    AccountRepository.getHash(),
                    idAnketaTaken,
                    body
                )
                db.odgovorDao().insertAll(Odgovor(idPitanje!!,odgovor, idPitanje))
                return@withContext sredjenProgres
            }
            catch(e: Exception ){
                return@withContext -1
            }
        }

    }

    fun sredi(progres: Double): Int {
        val povratni : Int= (progres*100).toInt()
        for (i in 90 downTo 0 step 20){
            if(i-povratni<10)
                return i+10
        }
        return 0
    }
}