package ba.etf.rma22.projekat


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import ba.etf.rma22.projekat.view.*
import ba.etf.rma22.projekat.viewmodel.PitanjeAnketaViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity(), Communicator {
    private lateinit var mPager: ViewPager2
    private lateinit var pagerAdapter : ViewPagerAdapter
    private lateinit var fragments : MutableList<Fragment>
    private val pitanjeAnketaViewModel = PitanjeAnketaViewModel()
    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    companion object{
        lateinit var mojContext: Context
        var konektovano: Boolean=true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mojContext = getApplicationContext()
        konektovano=isOnline(this)


        val payload = intent?.getStringExtra("payload")
            scope.launch{
                if(payload!=null)
                    AccountRepository.postaviHash(payload)
                else
                    AccountRepository.daSeNapraviBaza()
            }

        fragments = mutableListOf(FragmentAnkete.newInstance(), FragmentIstrazivanje.newInstance())
        if(!isOnline(this)){
            val bundle= Bundle()
            bundle.putString("poruka", "Nisam implementirala offline učitavanje anketa, ali jesam učitavanje grupa u fragmentu desno ->")
            val fragmentPoruka = FragmentPoruka.newInstance()
            fragmentPoruka.arguments=bundle
            fragments=mutableListOf(fragmentPoruka, FragmentIstrazivanje.newInstance())
        }
        mPager = findViewById(R.id.pager)
        pagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments, lifecycle)
        mPager.adapter = pagerAdapter



        val pageCallback =  object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int){
                if(position==0 && fragments[0] is FragmentAnkete) {
                    refreshSecondFragment(FragmentIstrazivanje.newInstance())
                }
                if(fragments[position] is FragmentPredaj)
                    pagerAdapter.refreshFragment(position, fragments[position])
            }
        }

        mPager.registerOnPageChangeCallback(pageCallback)
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    override fun passData(poruka: String) {
        val bundle= Bundle()
        bundle.putString("poruka", poruka)

        val fragmentPoruka = FragmentPoruka.newInstance()
        fragmentPoruka.arguments=bundle

        fragments = mutableListOf(FragmentAnkete.newInstance(), fragmentPoruka)
        pagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments, lifecycle)
        mPager.adapter = pagerAdapter
        mPager.setCurrentItem(1)

    }

    override fun passAnketa(idAnketa:Int, istrazivanje : String) {
        //var pitanja= listOf<Pitanje>()
        var i=0


        pitanjeAnketaViewModel.getPitanja(idAnketa){ pitanja->
            pagerAdapter.remove(1)
            pagerAdapter.remove(0)
            while(i<pitanja.size) {
                val bundle1 = Bundle()
                bundle1.putInt("pitanje", pitanja[i].id)
                bundle1.putInt("anketa", idAnketa)
                bundle1.putString("istrazivanje", istrazivanje)
                val fragmentPitanje=FragmentPitanje.newInstance()
                fragmentPitanje.arguments=bundle1
                pagerAdapter.add(i, fragmentPitanje)
                i++
            }

            val bundle = Bundle()
            bundle.putInt("anketa", idAnketa)
            bundle.putString("istrazivanje", istrazivanje)
            val fragmentPredaj = FragmentPredaj.newInstance()
            fragmentPredaj.arguments=bundle
            pagerAdapter.add(i, fragmentPredaj)
        }
    }

    override fun zaustavi() {
        fragments = mutableListOf(FragmentAnkete.newInstance(), FragmentIstrazivanje.newInstance())
        pagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments, lifecycle)
        mPager.adapter = pagerAdapter
    }

    private fun refreshSecondFragment(fragment : Fragment) {
        Handler(Looper.getMainLooper()).postDelayed({
            pagerAdapter.refreshFragment(1, fragment)
        }, 100)
    }

    inner class ViewPagerAdapter(
        fragmentManager: FragmentManager, var fragments: MutableList<Fragment>, lifecycle: Lifecycle
        ) : FragmentStateAdapter(fragmentManager, lifecycle) {

        override fun getItemCount(): Int {
            return fragments.size
        }
        override fun createFragment(position: Int): Fragment {
            return fragments[position]
        }

        fun add(index: Int, fragment: Fragment) {
            fragments.add(index, fragment)
            notifyItemChanged(index)
        }

        fun refreshFragment(index: Int, fragment: Fragment) {
            fragments[index] = fragment
            notifyItemChanged(index)
        }

        fun remove(index: Int) {
            fragments.removeAt(index)
            notifyItemChanged(index)
        }

        override fun getItemId(position: Int): Long {
            return fragments[position].hashCode().toLong()
        }

        override fun containsItem(itemId: Long): Boolean {
            return fragments.find { it.hashCode().toLong() == itemId } != null
        }
    }




}